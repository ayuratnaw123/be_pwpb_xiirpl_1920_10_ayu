<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


 
</center>
<div class="container">
  <div class="row mt-3">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header"> 
          Form ubah Data petugas
        </div>
        <div class="card-body">
          <form action="" method="post">
            <?php foreach($datainventariss as $di):?>
          <div class="form-group">
              <label for="id_peminjam">id peminjam</label>
              <input type="text" class="form-control" name="id_peminjam" id="id_peminjam" value="<?= $di['id_peminjam'];?>">
              <small class="form-text text-danger"><?= form_error('id_peminjam'); ?></small>
            </div>

            <div class="form-group">
              <label for="tanggal_pinjam">tanggal_pinjam</label>
              <input type="text" class="form-control" name="tanggal_pinjam" id="tanggal_pinjam" value="<?= $di['tanggal_pinjam'];?>">
              <small class="form-text text-danger"><?= form_error('tanggal_pinjam'); ?></small>
            </div>

            <div class="form-group">
              <label for="tanggal_kembali">tanggal_kembali</label>
              <input type="text" class="form-control" name="tanggal_kembali" id="tanggal_kembali" value="<?= $di['tanggal_kembali'];?>">
              <small class="form-text text-danger"><?= form_error('tanggal_kembali'); ?></small>
            </div>


            <div class="form-group">
              <label for="status_peminjaman">status peminjaman</label>
              <input type="text" class="form-control" name="status_peminjam" id="status_peminjaman" value="<?= $di['status_peminjaman'];?>">
              <small class="form-text text-danger"><?= form_error('status_peminjaman'); ?></small>
            </div>
           
            <button type="submit" name="ubah" class="btn btn-primary float-right">Ubah</button>
            <?php endforeach;?>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>