<!DOCTYPE html>
<html>
<head>
  <title>Tambah peminjaman</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
<style>
  #customer{
    display: none;
  }
  
  #kontak2{
    display: none;
  }
  
  #kontak3{
    display: none;
  }
  
  #kontak4{
    display: none; 
  }
</style>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
<div class ="container">
  <div class="row mt-3">
    <div class="col-md-6">




<div class="card">
  <div class="card-header">
    Form pengembalian
  </div>

  <div class="card-body">

    <?php if( validation_errors() ):?>
    <div class="alert alert-danger" role="alert">
      <?= validation_errors();?>
    </div>
  <?php endif;?>
    <form action="" method="post">
          
    
           
            <!-- 
             <div class="form-group">
              <label for="id_pengembalian">id pengembalian</label>
                 <input type="text" name="id_pengembalian" class="form-control" id="text">
                   </div> -->

            <div class="form-group">
              <label for="tanggal_kembali">tanggal kembali</label>
                 <input type="date" name="tanggal_kembali" class="form-control" id="text">
                   </div>

                    <div class="form-group">
              <label for="jumlah">jumlah</label>
                 <input type="number" name="jumlah" class="form-control" id="text">
                   </div>

                   <div class="form-group">
              <label for="id_peminjam">nama peminjam</label>
                 <select type="text" name="id_peminjam" class="form-control" id="text">
                 <?php foreach($id_peminjam as $id) : ?>
                  <option value="<?= $id['id_peminjam'] ?>"><?= $id['id_peminjam'] ?> - <?= $id['nama_peminjam'] ?></option>
                <?php endforeach; ?>
                   </div>
                </select>
                   <div class="form-group">
              <label for="id_peminjaman">status peminjaman peminjaman</label>
                 <select type="text" name="id_peminjaman" class="form-control" id="text">
                  <?php foreach($id_peminjaman as $id) : ?>
                  <option value="<?= $id['id_peminjaman'] ?>"><?= $id['id_peminjaman'] ?> - <?= $id['status_peminjaman'] ?></option>
                <?php endforeach; ?>
              </select>
                   </div>

                    <div class="form-group">
              <label for="id_inventaris">nama barang yang di pinjam</label>
                 <select name="id_inventaris" class="form-control" id="text">
                 <?php foreach($id_inventaris as $id) : ?>
                  <option value="<?= $id['id_inventaris'] ?>"><?= $id['id_inventaris'] ?> - <?= $id['nama'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>

                   </div>


             

                   
              </div>
               <button type="submit" name="tambah" class="btn btn-primary" style="margin-top: 30px;">Tambah Data</button>

                   </form>
               </div>
           </div>
       </div>
   </div>
</div>

                           

               
  