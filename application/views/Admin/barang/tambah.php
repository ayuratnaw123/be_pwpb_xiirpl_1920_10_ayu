<!DOCTYPE html>
<html>
<head>
  <title>Tambah barang</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
</head>
<body style="background-color: teal;"> 
<style>
  #customer{
    display: none;
  }
  
  #kontak2{
    display: none;
  }
  
  #kontak3{
    display: none;
  }
  
  #kontak4{
    display: none; 
  }
</style>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
<div class ="container">
  <div class="row mt-3">
    <div class="col-sm-12">




<div class="card">
  <div class="card-header" style="background-color: black; color : white; text-align: center;">
    Form petugas
  </div>

  <div class="card-body">

    <?php if( validation_errors() ):?>
    <div class="alert alert-danger" role="alert">
      <?= validation_errors();?>
    </div>
  <?php endif;?>
    <form action="<?= base_url(); ?>barang/tambah" method="post" enctype="multipart/form-data">
             <div class="form-group">
              <label for="id_ruang"><b>id nama ruang </label>
                 <select type="text" name="id_ruang" class="form-control" id="text">
                  <?php foreach($id_ruang as $id) : ?>
                  <option value="<?= $id['id_ruang'] ?>"><?= $id['id_ruang'] ?> - <?= $id['nama_ruang'] ?></option>
                <?php endforeach; ?>
              </div>
            </select>
            <div class="form-group">
              <label for="id_jenis">nama jenis barang</label>
                 <select type="text" name="id_jenis" class="form-control" id="text">
                   <?php foreach($id_jenis as $id) : ?>
                  <option value="<?= $id['id_jenis'] ?>"><?= $id['id_jenis'] ?> - <?= $id['nama_jenis'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="custom-file">
                <input type="file" name="gambar" value="input gambar" class="custom-file-input font-default" id="gambar">
                <label class="custom-file-label" for="gambar">Gambar barang</label>
            </div>
            <div class="form-group">
              <label for="nama">nama barang</label>
                 <input type="text" name="nama" class="form-control" id="text">
             </div>
              <div class="form-group">
                <label for="kondisi">kondisi</label>
                <input type="text" name="kondisi" class="form-control" id="text">
              </div>
              <div class="form-group">
                <label for="jumlah">jumlah</label>
                <input type="number" name="jumlah" class="form-control" id="text">
              </div>
              <div class="form-group">
                <label for="kode_inventaris">kode inventaris</label>
                <input type="text" name="kode_inventaris" class="form-control" id="text">
              </div>
              <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <input type="text" name="keterangan" class="form-control" id="text">
              </div>
              <!-- </div> -->
               <button type="submit" name="tambah" class="btn btn-primary" style="margin-top: 30px;">Tambah Data</button>
          </form>
               </div>
           </div>
       </div>
   </div>
</div>

                           

               