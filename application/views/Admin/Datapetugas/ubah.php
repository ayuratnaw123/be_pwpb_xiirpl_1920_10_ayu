<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

 

</center>
<div class="container">
  <div class="row mt-3">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          Form Tambah Data petugas
        </div>
        <div class="card-body">
          <form action="" method="post">
            <input type="hidden" name="id" value="<?= $ptgs->id_petugas; ?>">
          <div class="form-group">
              <label for="id_level">id level</label>
              <input type="text" class="form-control" name="id_level" id="id_level" value="<?= $ptgs->id_level; ?>">
              <small class="form-text text-danger"><?= form_error('id_level'); ?></small>
            </div>

            <div class="form-group">
              <label for="id_petugas">id petugas</label>
              <input type="text" class="form-control" name="id_petugas" id="id_petugas" value="<?= $ptgs->id_petugas; ?>">
              <small class="form-text text-danger"><?= form_error('id_petugas'); ?></small>
            </div>

            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" name="username" id="username" value="<?= $ptgs->username; ?>">
              <small class="form-text text-danger"><?= form_error('username'); ?></small>
            </div>


            <div class="form-group">
              <label for="password">password</label>
              <input type="text" class="form-control" name="password" id="password" value="<?= $ptgs->password; ?>">
              <small class="form-text text-danger"><?= form_error('password'); ?></small>
            </div>

            <div class="form-group">
              <label for="nama_ptgs">nama petugas</label>
              <input type="text" class="form-control" name="nama_ptgs" id="nama_ptgs" value="<?= $ptgs->nama_ptgs; ?>">
              <small class="form-text text-danger"><?= form_error('nama_ptgs'); ?></small>
            </div>

            <div class="form-group">
              <label for="Email">Email</label>
              <input type="text" class="form-control" name="Email" id="Email" value="<?= $ptgs->Email; ?>">
              <small class="form-text text-danger"><?= form_error('Email'); ?></small>
            </div>

            <!-- <div class="form-group">
              <label for="jurusan">Jurusan</label>
              <select class="form-control" name="jurusan" id="jurusan">
                <?php foreach( $jurusan as $j ) : ?>
                  <?php if( $j == $mahasiswa['jurusan'] ) : ?>
                    <option value="<?= $j; ?>" selected><?= $j; ?></option>
                  <?php else : ?>
                    <option value="<?= $j; ?>"><?= $j; ?></option> -->
               <!--    <?php endif; ?>
                <?php endforeach; ?> -->
              </select>
                <small class="form-text text-danger"><?= form_error('jurusan'); ?></small>
            </div>
            <button type="submit" name="ubah" class="btn btn-primary float-right">Ubah</button>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>