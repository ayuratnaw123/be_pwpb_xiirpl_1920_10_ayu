<!doctype html>
<html lang="en">
<head>



		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>admin</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/theme.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/theme-elements.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/theme-blog.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/css/vendor/theme-shop.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?= base_url();?>assets/css/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?= base_url();?>assets/css/css/custom.css">

		<!-- Head Libs -->
		<script src="<?= base_url();?>assets/css/vendor/modernizr/modernizr.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



      

      
      
</head>
<body>
        
               <style>
        
        body{
            margin: 0px;
            padding: 0px;
            background-color:white;
        }
        .image-top{
            width: 100%;
            height:70vh;
            background-image: url('<?= base_url();?>assets/img/8999.jpg');
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            
         }
         .lapisanimage{
             width: 100%;
             height:70vh;
             background-color: rgba(44, 39, 40, 0.70);
           margin-top: -35px;
            }
            .txt-it1{
               color: #fff;
               font-family: informal roman;
               padding-top: 150px;
               padding-left:px;
               font-size: 60px; 
               text-decoration: none; 
            }

              .txt-it2{
                  color: #fff;
                  font-family: informal roman;
                  padding-left:-20px; 
                  font-size:40pt;
                  width: 550px;
                  margin-top: -20px;
             }


             img{

               width: 400px;
               float :right;
             }

            .g{
              width:98%;
            
              background-color: white ;
              margin-top:10%;
              border-radius:3%;
              padding: 8%;
              box-shadow: 0px 7px 40px; 

            }

             .imag-top{
            width: 100%;
            height:40vh;
            background-image: url('../assets/img/8989.png');
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            px;
         }
         .lapisanimag{
             width: 100%;
             height:40vh;
             background-color: rgba(44, 39, 30, 0.50);
             margin-top: -10px;
            }

              .txt-it1{
               color: #fff;
               font-family: informal roman;
               padding-top: 125px;
               padding-left:px;
               font-size: 50px; 
               text-decoration: none; 
            }

            .pembungkus {
    margin-top: 20px;
    width: 100%;
    height: auto;
    background-color: white;
    border-radius: 8px;
    padding: 15px;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-evenly;
   }

   .profile {
    width: 600px;
    height: auto;
    background-color: #171717;
    box-shadow: 0px 3px 15px rgb(214, 214, 214);
    padding: 5px 10px;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-evenly;
    margin-right: 10%;
    color: white;
    font-family:calibri;
   }

    

    .bungkus {
    margin-top: -14px;
    width: 100%;
    height: auto;
    background-color: white;
    border-radius: 8px;
    padding: px;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-evenly;
   }

    .profili {
    width: 600px;
    height: auto;
    background-color:#171717;
    box-shadow: 0px 3px 15px rgb(214, 214, 214);
    padding: 5px 10px;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-evenly;
    margin-left: 7%;
    color: white;
    font-family:white;
   }

   .bungkusin {
   
    width: 100%;
    height: auto;
    background-color: white;
    border-radius: 8px;
    padding: 15px;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-evenly;
   }

   #header .header-nav-main nav > ul > li.open > a, #header .header-nav-main nav > ul > li:hover > a {
    background: #0e0e0e;
   }

  

.bungkus1{
  background-color: white;
  width: 200px;
  height: 250px;
}
.bungkus1 img{
  width: 50%;
  margin-right:40%;

}
     .d{

       font-size:50pt;
       color : grey;
       font-family: informal roman;
       
     }

     .page-header {
    background-color: #171717;
    border-bottom: 10px solid gray;
    border-top: 5px solid #384045;
    margin: 0 0 35px 0;
    min-height: 50px;
    padding: 20px 0;
    position: relative;
    text-align: left;
     }
   
             
           </style>
           <div class="body">
            <header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 57, 'stickySetTop': '-57px', 'stickyChangeLogo': true}">
              <div class="header-body">
                <div class="header-container container">
                  <div class="header-row">
                    <div class="header-column">
                      <div class="header-logo">
                        <a href="index.html">
                          <img src = "<?= base_url();?>assets/img/inx.png" style="width:150px;">
                        </a>
                      </div>
                    </div>
                    <div class="header-column">
                      <div class="header-row">
                        <div class="header-search hidden-xs">
                          <form id="searchForm" action="page-search-results.html" method="get">
                            <div class="input-group">
                              <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                              </span>
                            </div>
                          </form>
                        </div>
                        <nav class="header-nav-top">
                          <ul class="nav nav-pills">
                            <li class="hidden-xs">
                              <a href="about-us.html"><i class="fa fa-angle-right"></i> About Us</a>
                            </li>
                            <li class="hidden-xs">
                              <a href="contact-us.html"><i class="fa fa-angle-right"></i> Contact Us</a>
                            </li>
                            <li>
                              <span class="ws-nowrap"><i class="fa fa-phone"></i> 0896-5084-1048</span>
                            </li>
                          </ul>
                        </nav>
                      </div>
                      <div class="header-row">
                        <div class="header-nav">
                          <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                            <i class="fa fa-bars"></i>
                          </button>
                          <ul class="header-social-icons social-icons hidden-xs">
                            <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                          </ul>
                          <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                            <nav>
                              <ul class="nav nav-pills" id="mainNav">
                                
                                  
                              
                               
                                  
                                <li class="dropdown">
                                  <a class="dropdown-toggle" href="<?= base_url();?>datapetugas" style="color:  teal;">
                                  Petugas
                                  </a>


                                <li class="dropdown">
                                  <a class="dropdown-toggle" href="<?= base_url();?>datapegawai" style =" color: teal">
                                   Pegawai
                                  </a>

                                   

                                 
                                  
                                <li class="dropdown">
                                  <a class="dropdown-toggle" href="<?= base_url(); ?>Auth/logout" style=" color: teal;">
                                    keluar
                                  </a>
                                  
                                  
                                  </ul>
                                </li>
                              </ul>
                            </nav>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </header>
      
            <div role="main" class="main">
      
              <section class="page-header">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <ul class="breadcrumb">
                        
                        
                      </ul>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h1>Admin</h1>
                    </div>
                  </div>
                </div>
              </section>

                    <div class="image-top">
        <div class="lapisanimage">
          <center><h1 class="txt-it1"style="margin-top:20px;"><b>SELAMAT DATANG</h1><br>
            <h3 class="txt-it2" "><b>ADMIN</h3>
              
              </center>
            </div>
          </div>


          <br<br><br><br><br>

         <center><div class="container">
            <div class="row">
              <div class="col-md-15">
                <div class="row">
                  <ul class="portfolio-list">
                    <li class="col-md-3 col-sm-6 col-xs-12">
                      <div class="portfolio-item">
                       
                          <span class="thumb-info thumb-info-lighten">
                            <span class="thumb-info-wrapper">
                              <img src="<?= base_url();?>assets/img/opendatakit.png" class="img-responsive" alt="">
                              <span class="thumb-info-title">
                                <span class="thumb-info-inner" style="width:200px; height:40px; font-size:15pt;">Data Peminjaman</span>
                                  </span>
                                  
                             </span>
                          </span>
                         <a href="<?= base_url();?>datainventaris"><button type="button" class="btn btn-outline-primary" style="margin-top:10px;margin-left:10px; width:230px; height:40px; font-size:10pt; background-color: #0e0e0e; color: white;">MASUK
                         </button>
                        </a>
                      </a>
                  </div>
                </li>
  
  
               
              

          
          
  
  
                  <li class="col-md-3 col-sm-6 col-xs-12">
                      <div class="portfolio-item">
                        
                          <span class="thumb-info thumb-info-lighten">
                            <span class="thumb-info-wrapper">
                              <img src="<?= base_url();?>assets/img/8960.png" class="img-responsive" alt="">
                              <span class="thumb-info-title">
                                <span class="thumb-info-inner"style="width:200px; height:40px; font-size:15pt;">DATA PENGEMBALIAN</span>
                               
                              </span>
                              <span class="thumb-info-action">
                              
                              </span>
                            </span>
                          </span>
                             
                          <a href="<?= base_url();?>pengembalian"><button type="button" class="btn btn-outline-primary" style="margin-top:10px;margin-left:10px; width:230px; height:40px; font-size:10pt; background-color: #0e0e0e; color :white;">MASUK
                          </button>
                        </a>
                      </a>
                      </div>
                    </li>
  
                  
                  
                    <li class="col-md-3 col-sm-6 col-xs-12">
                        <div class="portfolio-item">
                          
                         <span class="thumb-info thumb-info-lighten">
                              <span class="thumb-info-wrapper">
                                <img src="<?= base_url();?>assets/img/hkj.png" class="img-responsive" alt="">
                                <span class="thumb-info-title">
                                  <span class="thumb-info-inner"style="width:200px; height:40px; font-size:15pt">Data Barang</span>
                                 
                                </span>
                                <span class="thumb-info-action">
                               
                                </span>
                              </span>
                            </span>
    
                            <a href="<?= base_url();?>barang"><button type="button" class="btn btn-outline-primary" style="margin-top:10px;margin-left:10px; width:230px; height:40px; font-size:10pt; background-color: #0e0e0e; color :white;">MASUK
                            </button>
                            </button>
                                                </a>
                                              </a>
                                          </div>
                                        </li> 


                               <li class="col-md-3 col-sm-6 col-xs-12">
                    <div class="portfolio-item">
                      
                        <span class="thumb-info thumb-info-lighten">
                          <span class="thumb-info-wrapper">
                            <img src="<?= base_url();?>assets/img/55.jpg" class="img-responsive" alt="">
                            <span class="thumb-info-title">
                              <span class="thumb-info-inner"style="width:200px; height:40px; font-size:15pt;">Ruang</span>
                             
                            </span>
                            <span class="thumb-info-action">
                            
                            </span>
                          </span>
                        </span>
                           
                       <a href="<?= base_url();?>ruang"><button type="button" class="btn btn-outline-primary" style="margin-top:10px;margin-left:10px; width:230px; height:40px; font-size:10pt; background-color: #0e0e0e; color : white;">MASUK
                            </button>
                            </button>
                      </a>
                    </a>
                    </div>
                  </li>   
                        </a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  
<br><br><br><br><br>


                <center><b style="font-family: informal roman; font-size:30pt; color : #0e0e0e;">ABOUT US</b></center><br><br>

                     <div class="pembungkus">
                        <img style="float: left;  margin-left: 14.2%; width: 35%; height: auto;" src ="<?= base_url();?>assets/img/2.jpeg">
                        <div class ="profile" >
                            <div class ="t" style="margin-top : 10%;"><p><center>Lorem ipsum dolor sit amet consectetur 
                              adipisicing elit. <br>
                              Lorem ipsum dolor sit amet consectetur 
                              adipisicing elit. <br> 
                              Lorem ipsum dolor sit amet consectetur 
                              adipisicing elit. <br>
                              Lorem ipsum dolor sit amet consectetur 
                              adipisicing elit. <br>
                              Lorem ipsum dolor sit amet consectetur 
                              adipisicing elit. <br>
                                </center>
                                </p>
                            </div>
                        </div>
                     </div>


                     <div class="bungkus"> 
                        <div class ="profili" >
                            <div class ="t" style="margin-top : 10%;"><p><center>Lorem ipsum dolor sit amet consectetur 
                                adipisicing elit. <br>
                                Lorem ipsum dolor sit amet consectetur 
                                adipisicing elit. <br> 
                                Lorem ipsum dolor sit amet consectetur 
                                adipisicing elit. <br>
                                Lorem ipsum dolor sit amet consectetur 
                                adipisicing elit. <br>
                                Lorem ipsum dolor sit amet consectetur 
                                adipisicing elit. <br>
                                </center>
                               </p>
                            </div>
                        </div>
                        <img style="float: left; margin-right: 18%; width: 35%; height: auto;" src ="<?= base_url();?>assets/img/login.jpeg">
                     </div>
                     <br><br><br>

                     <footer id="footer">
                      <div class="container">
                        <div class="row">
                          <div class="footer-ribbon">
                            <span>Get in Touch</span>
                          </div>
                          <div class="col-md-3">
                            <div class="newsletter">
                              <h4>Newsletter</h4>
                              <p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>
                    
                              <div class="alert alert-success hidden" id="newsletterSuccess">
                                <strong>Success!</strong> You've been added to our email list.
                              </div>
                    
                              <div class="alert alert-danger hidden" id="newsletterError"></div>
                    
                              <form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
                                <div class="input-group">
                                  <input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Go!</button>
                                  </span>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <h4></h4>
                            <div id="tweet" class="twitter" data-plugin-tweets data-plugin-options="{'username': '', 'count': 2}">
                              <p></p>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="contact-details">
                              <h4>Contact Us</h4>
                              <ul class="contact">
                                <li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>
                                <li><p><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-789</p></li>
                                <li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <h4>Follow Us</h4>
                            <ul class="social-icons">
                              <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                              <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                              <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="footer-copyright">
                        <div class="container">
                          <div class="row">
                            <div class="col-md-1">
                              <a href="index.html" class="logo">
                                
                              </a>
                            </div>	
                            <div class="col-md-7">
                              <center><p>© Copyright Ratnaayu</p></center>
                            </div>
                            </div>
                            </div>
                            </div>
                    </div></div>
                       
                     

                                
                            
   <!-- Vendor -->
   <script src="<?= base_url();?>assets/css/vendor/jquery/jquery.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/jquery.appear/jquery.appear.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/jquery.easing/jquery.easing.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/jquery-cookie/jquery-cookie.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/bootstrap/js/bootstrap.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/common/common.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/jquery.validation/jquery.validation.min.js"></script>
   <script src="assets/css/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/jquery.gmap/jquery.gmap.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/isotope/jquery.isotope.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/owl.carousel/owl.carousel.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
   <script src="<?= base_url();?>assets/css/vendor/vide/vide.min.js"></script>


  
    

    