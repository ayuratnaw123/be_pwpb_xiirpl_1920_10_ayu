 <?php

class Ruang_model extends CI_Model {

   public function getAllRuang()
   {
 
   	return $this->db->get('ruang')->result_array();
   } 


   public function tambahruang()
	
{
		$data = [

			
			"nama_ruang"       => $this->input->post('nama_ruang', true),
			"kode_ruang"    => $this->input->post('kode_ruang', true)
			

		];

		$this->db->insert('ruang', $data); 


 	}

 	public function cariDataruang()
 	{


         $keyword = $this->input->post('keyword', true);
         $this->db->like('nama_ruang', $keyword);
         return $this->db->get('ruang')->result_array();
 	}

public function hapusDataruang($id_ruang)
 	{
 		$this->db->where('id_ruang', $id_ruang);
 		$this->db->delete('ruang');
 	}

 
 }