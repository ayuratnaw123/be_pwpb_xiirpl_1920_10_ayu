 <?php
 

class Datapetugas_model extends CI_model {
	public function getAlldatapetugas()
	{
		return $this->db->get('petugas')->result_array();
	}
 
	public function hapusDatapetugas($id_petugas)
 	{
 		$this->db->where('id_petugas', $id_petugas);
 		$this->db->delete('petugas');
 	}
 

 	public function tambahdatapetugas()
	
{
		$data = [

			
			"id_level"    => $this->input->post('id_level', true),
			"username"    => $this->input->post('username', true),
			"password"    => $this->input->post('password', true),
			"nama_ptgs"   => $this->input->post('nama_ptgs', true),
			"Email"   => $this->input->post('Email', true)

		];

		$this->db->insert('petugas', $data); 


 	}

 	public function cariDataPetugas()
 	{


         $keyword = $this->input->post('keyword', true);
         $this->db->like('nama_ptgs', $keyword);
         return $this->db->get('petugas')->result_array();
 	}

 	public function getDatapetugasById($id_petugas)
 	{
 			//ar_dump($id_petugas);die();
           return $this->db->get_where('petugas', ['id_petugas' => $id_petugas]);
 	}


 	public function ubah()
        {
            $data = [
                "id_level" => $this->input->post('id_level', true),
                "id_petugas" => $this->input->post('id_petugas',true),
                "username" => $this->input->post('username',true),
                "password" => $this->input->post('password', true),
                "nama_ptgs" => $this->input->post('nama_ptgs', true),
                "Email" => $this->input->post('Email', true)
            ];
            
            $this->db->where('id_petugas', $this->input->post('id_petugas'));
            $this->db->update('petugas', $data);
        }

        public function getidlevel()
    {

        $this->db->select('id_level');
        // $this->db->select('nama_level');
        $this->db->from('petugas');
        $result = $this->db->get()->result_array();
        return $result;
    }

     public function getnamalevel()
    {

        $this->db->select('nama_level');
        $this->db->from('level');
        $result = $this->db->get()->result_array();
        return $result;
    }
 }
