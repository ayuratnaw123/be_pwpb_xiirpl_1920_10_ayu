<?php
  
class Barang_model extends CI_model{
	public function getAllbarang()
	{
		return $this->db->get('inventaris')->result_array();
	} 
  

	public function tambahdatabarang()	
	{
 		$data = [ 

            "id_jenis"    => $this->input->post('id_jenis', true),
			"id_ruang"    => $this->input->post('id_ruang', true),
			"nama"    => $this->input->post('nama', true),
			"kondisi"    => $this->input->post('kondisi', true),
			"jumlah"    => $this->input->post('jumlah', true),
			"kode_inventaris"   => $this->input->post('kode_inventaris', true),
			"keterangan"   => $this->input->post('keterangan', true)

		];

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $this->input->post('id_inventaris');

		$this->load->library('upload', $config);
		
		if( ! $this->upload->do_upload('gambar') ) {

			$error = ['error' => $this->upload->display_error()];
			$this->load->view('Barang/tambah', $error);

		} else{

			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];
			$data['gambar'] = 'uploads/'.$file_name;
		 	if($this->db->insert('inventaris', $data)) {
		   	redirect('barang');
			 
			}
		}
	}


	// }
	// 	}


	// 	$this->db->insert('inventaris', $data); 


 // 	}

 	public function hapusDatabarang($id_inventaris)
 	{
 		$this->db->where('id_inventaris', $id_inventaris);
 		$this->db->delete('inventaris');
 	}

 	public function getBarangById($id_inventaris)
 	{
           return $this->db->get_where('inventaris', ['id_inventaris' => $id_inventaris])->result_array();
 	}


 	public function cariDatabarang()
 	{


         $keyword = $this->input->post('keyword', true);
         $this->db->like('nama', $keyword);
         $this->db->or_like('jumlah', $keyword);
         return $this->db->get('inventaris')->result_array();
 	}

 	public function getidruang()
	{

		$this->db->select('id_ruang');
		$this->db->select('nama_ruang');
        $this->db->from('ruang');
		$result = $this->db->get()->result_array();
		return $result;
	}

	 public function getnamaruang()
	{

		$this->db->select('nama_ruang');
		$this->db->from('ruang');
		$result = $this->db->get()->result_array();
		return $result;
	}
 

     public function getidjenis()
	{

		$this->db->select('id_jenis');
		$this->db->select('nama_jenis');
		$this->db->from('jenis');
		$result = $this->db->get()->result_array();
		return $result;
	}

     public function getnamajenis()
	{

		$this->db->select('nama_jenis');
        $this->db->from('jenis');
		$result = $this->db->get()->result_array();
		return $result;
	}

	public function ubah()
        {

        	$id = $this->input->post('id_inventaris');

            if($_FILES['image']['name']!=""){
            	// $data['gambar'] = $this->_uploadImage();
            	$config['upload_path'] = './uploads';
            	$config['allowed_types'] = 'gif/jpg/jpeg/png';
            	$this->load->library('upload', $config);
            	if($this->upload->do_upload('image'))
            	{
            		$uploadData = $this->upload->data();
	            	$image = $uploadData['file_name'];
	            }else{
            	// $data['gambar'] = $this->input->post('old_image', true);
    	        	$image = '';
            	}
            }else{
            	$image = '';
            }

            $data = [
            "id_jenis"    => $this->input->post('id_jenis', true),
			"id_ruang"    => $this->input->post('id_ruang', true),
			"nama"    => $this->input->post('nama', true),
			"kondisi"    => $this->input->post('kondisi', true),
			"jumlah"    => $this->input->post('jumlah', true),
			"kode_inventaris"   => $this->input->post('kode_inventaris', true),
			"keterangan"   => $this->input->post('keterangan', true)
            ];

            if($image != '')
            {
            	$data['gambar'] = $image;
            	unlink("./uploads/$row->file_name");
            }
            // var_dump($data);
            // var_dump($this->input->post('gambar', true));
            
        // $config['upload_path'] = './uploads/';
        // $config['allowed_types'] = 'jpg|jpeg|png';
        // $config['file_name'] = $this->input->post('id_inventaris');
        // $config['overwrite'] = true;

        // $this->load->library('upload', $config);

        // if(!$this->upload->do_upload('gambar') ) {
        //     $error = ['error' => $this->upload->display_errors()];
            //echo "ngga aupload";
            // $this->load->view('admin/barang/index', $error);
        // } else {
        //     $upload_data = $this->upload->data();
        //     $file_name = $upload_data['file_name'];
            //$data['gambar'] = 'uploads/'.$file_name;
            // $data['gambar'] = "sfjsakvsf";

             $this->db->where('id_inventaris', $this->input->post('id_inventaris'));
             if($this->db->update('inventaris', $data)) {
                 redirect('barang');
             } 



        // }

 			           
             // $this->db->where('id_inventaris', $this->input->post('id_inventaris'));
             // $this->db->update('inventaris', $data);
        }

        public function _uploadImage()
        {
	        $config['upload_path'] = './uploads/';
	        $config['allowed_types'] = 'jpg|jpeg|png';
	        //$config['file_name'] = 'sfd';
	        $config['overwrite'] = true;

	        $this->load->library('upload', $config);

	        if(!$this->upload->do_upload('gambar')) {
	        	$error = array('error' => $this->upload->display_errors());
	        	return "databeradasdas";
	        }
	        else{
	        	$data = array('upload_data' => $this->upload->data());
	         	return "berhasil!!";
	        }
	    }



} 
?>