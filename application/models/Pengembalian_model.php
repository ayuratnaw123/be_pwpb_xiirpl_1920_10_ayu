<?php
 
class Pengembalian_model extends CI_model { 
	public function getAllPengembalian()
	{
		return $this->db->get('ppengembalian')->result_array();
	} 

     public function hapuspengembalian($id_pengembalian)
 	{
 		$this->db->where('id_pengembalian', $id_pengembalian);
 		$this->db->delete('pengembalian');
 	}

           public function tambahdatapengembalian()
	
{ 
		$data = [

			// "id_pengembalian" => $this->input->post('id_pengembalian', true),
			"tanggal_kembali" => $this->input->post('tanggal_kembali', true),
			"jumlah" => $this->input->post('jumlah', true),
			"id_peminjam"=> $this->input->post('id_peminjam', true),
		    "id_peminjaman"=> $this->input->post('id_peminjaman', true),
		    "id_inventaris"=> $this->input->post('id_inventaris', true)
			


			
		 	
			
		];

		$this->db->insert('pengembalian', $data); 


 	}

 	public function caripengembalian()
 	{


         $keyword = $this->input->post('keyword', true);
         $this->db->or_like('tanggal_kembali', $keyword);
         return $this->db->get('pengembalian')->result_array();
 	}

 	public function getpengembalianById($id_pengembalian)
 	{
           return $this->db->get_where('ppengembalian', ['id_pengembalian' => $id_pengembalian])->result_array();
 	}


    public function getidinventaris()
	{

		$this->db->select('id_inventaris');
		$this->db->select('nama');

		$this->db->from('inventaris');
		$result = $this->db->get()->result_array();
		return $result;
	}
      
      public function getnama()
	{

		$this->db->select('nama');
		$this->db->select('nama');

		$this->db->from('inventaris');
		$result = $this->db->get()->result_array();
		return $result;
	}     

     public function getidpeminjam()
	{

		$this->db->select('id_peminjam');
		$this->db->select('nama_peminjam');

		$this->db->from('peminjam');
		$result = $this->db->get()->result_array();
		return $result;
	}
     

      public function getidpeminjaman()
	{

		$this->db->select('id_peminjaman');
		$this->db->select('status_peminjaman');

		$this->db->from('peminjaman');
		$result = $this->db->get()->result_array();
		return $result;
	} 


	  public function getnamapeminjaman()
	{

		$this->db->select('nama_peminjaman');
		$this->db->from('peminjam');
		$result = $this->db->get()->result_array();
		return $result;
	}

	 public function getstatuspeminjaman()
	{

		$this->db->select('status_peminjaman');
		$this->db->from('peminjaman');
		$result = $this->db->get()->result_array();
		return $result;
	}        
}