<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class datapegawai extends CI_Controller {
	public function __construct()
	{

		parent :: __construct();
		$this->load->model('Pegawai_model');
	 	$this->load->library('form_validation');
 
	}

    public function index()
	{
		
		$data['judul'] = 'data pegawai';
		$data['pegawai'] = $this->Pegawai_model->getAllpegawai();
		if( $this->input->post('keyword')){
			$data['pegawai'] = $this->Pegawai_model->cariDataPegawai();
		}

		$this->load->view('Admin/pegawai/index', $data);
	}

	public function tambah()

	{

       $data['judul']   = 'form Tambah Data pegawai';
      
   
        
        $this->form_validation->set_rules('nama_peminjam', 'Nama_peminjam', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('Email', 'Email', 'required');


      if( $this->form_validation->run() == FALSE ) {

      	$this->load->view('Admin/pegawai/tambah');

      }else{

      	$this->Pegawai_model->tambahdatapegawai();
      	//$this->Datainventaris_model->inventaris();
      	redirect('datapegawai');
      }
		
   }

   public function hapus($id_peminjam)
	{
		$this->Pegawai_model->hapusDatapegawai($id_peminjam);
		redirect('datapegawai');
	}


	public function detail($id_peminjam) {
	  	$data['judul'] = 'Detail Data peminjam';
		$data['datapegawai'] = $this->Pegawai_model->getpegawaiById($id_peminjam); 
		// var_dump( $id) ;die();
			$this->load->view('Admin/pegawai/detail', $data);
		}



		public function ubah($id_peminjam)
		{	
			$data['judul'] = 'Form Ubah Data peminjam';
			$data['pm']    = $this->Pegawai_model->getPegawaiById($id_peminjam);
			 //var_dump($data['pm']);die();
			
			$this->form_validation->set_rules('id_peminjam', 'id_peminjam', 'required');
			$this->form_validation->set_rules('nama_peminjam', 'Nama_peminjam', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('Email', 'Email', 'required');

			if( $this->form_validation->run() == FALSE ) {
			$this->load->view('Admin/pegawai/ubah',$data);
		} else{
			$this->Pegawai_model->ubah();
		    // $this->session->set_flashdata('flash', 'Diubah');
			redirect('datapegawai');
		}
	}
}
     
 