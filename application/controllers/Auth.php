<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends CI_Controller {
    

    public function __construct(){

    parent::__construct();
    $this->load->library('form_validation');
    $this->db2 = $this->load->database('admin',TRUE);
    $this->load->library('session');
    $this->load->model('Barang_model');
    }

// public function index()
// {
//     $this->form_validation->set_rules('username','Username','required|trim');
//     $this->form_validation->set_rules('password','Password','required|trim');
// if ($this->form_validation->run() !== false){
//     $data['title'] = 'inverst';
//      $this->load->view('login/index');

   
// }else{
//     $this->aksi_login();
    
// }

// }

public function aksi_login()
{
    $username = $this->input->post('username');
    $password = $this->input->post('password');

    $user = $this->db2->get_where('petugas',['username' => $username])->row_array();
    
    if ($user) {
        if($password == $user['password'] && $user['id_level'] == 1) {
            $data = [
                'username' => $user['username'],
                'id_level' => $user['id_level']
            ];
            $this->session->set_userdata($data);
            redirect('auth/admin');
            }
            elseif($password == $user['password'] && $user['id_level'] == 2){
            $data = [
                'username' => $user['username'],
                'id_level' => $user['id_level']
            ];
            $this->session->set_userdata($data);
            redirect('auth/petugas');
            }
            else{
                redirect('auth/signup');
        }
    }else{

         $userpegawai = $this->db->get_where('peminjam',['username' => $username])->row_array();
       if ($userpegawai) {
        if($password == $userpegawai['password']){
            $data = [
                'username' => $userpegawai['username'],
                'id_level' => $userpegawai['id_level']
            ];
            $this->session->set_userdata($data);
            redirect('auth/pegawai');
    }else{
                echo "gagal";
        } 
     }
   }
}  

function admin(){
    $this->load->view('admin/index');
}


function pegawai(){
    $data['judul'] = 'data barang';
    $data['barang'] = $this->Barang_model->getAllbarang();
    
    $this->load->view('listbarang/index', $data);
}

function petugas(){
    $this->load->view('Admin/petugas/index');
}

   function logout(){
        $this->session->sess_destroy();
        $url=base_url('');
        redirect('home');
    }

}

