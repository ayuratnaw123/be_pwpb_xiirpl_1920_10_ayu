<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class datainventaris extends CI_Controller {
	 public function __construct() 
	 {
	 	parent::__construct();
	 	$this->load->model('Datainventaris_model');
	 	$this->load->library('form_validation'); 
	 }  
  
    
 
	 public function index()
	{
		
		$data['judul'] = 'datainventariss';
		$data['datainventariss'] = $this->Datainventaris_model->getAllDatainventariss();
		if( $this->input->post('keyword')){
			$data['datainventariss'] = $this->Datainventaris_model->cariDatapeminjam();
		}


		$this->load->view('Admin/inventaris/index', $data);
	}


	public function tambah()

	{

       $data['judul']   = 'form Tambah Data inventaris';
       $data['id_peminjam'] = $this->Datainventaris_model->getidpeminjam();
       $data['id_inventaris'] = $this->Datainventaris_model->getidinventaris();
       $data['nama'] = $this->Datainventaris_model->getnama();
        $data['id_peminjaman'] = $this->Datainventaris_model->getidpeminjaman();
       
       
       

   
       // $this->form_validation->set_rules('id_peminjam', 'Id_peminjam', 'required');
       //  $this->form_validation->set_rules('tanggal_pinjam', 'Tanggal_pinjam', 'required');
       //  $this->form_validation->set_rules('tanggal_kembali', 'Tanggal_kembali', 'required');
        $this->form_validation->set_rules('id_inventaris', 'id_inventaris', 'required');
        $this->form_validation->set_rules('id_peminjaman', 'id_peminjaman', 'required');
          $this->form_validation->set_rules('id_detail_pinjam', 'id_detail_pinjam', 'required');
        $this->form_validation->set_rules('jumlah_pinjam', 'jumlah_pinjam', 'required');


      if( $this->form_validation->run() == FALSE ) {

      	$this->load->view('Admin/inventaris/Tambah', $data);

      }else{

      	$this->Datainventaris_model->tambahdatainventaris();
      	// $this->Datainventaris_model->tambahpeminjam();
      	redirect('peminjamanpetugas');
      }
		
   } 

	public function detail($id) {
	  	$data['judul'] = 'Detail Data inventaris';
		  $data['datainventariss'] = $this->Datainventaris_model->getDatainventarissById($id); 
		// var_dump( $id) ;die();
			$this->load->view('Admin/inventaris/detail', $data);
		}
      

		public function hapus($id_detail_pinjam)
	{
		$this->Datainventaris_model->hapusDatainventaris($id_detail_pinjam);
		redirect('datainventaris');
	}



   public function ubah($id_peminjaman)

	{

       $data['judul']   = 'ubah Data inventaris';
       $data['datainventariss'] = $this->Datainventaris_model->getPeminjamanById($id_peminjaman);
       

   
        $this->form_validation->set_rules('id_peminjam', 'Id_peminjam', 'required');
        $this->form_validation->set_rules('tanggal_pinjam', 'Tanggal_pinjam', 'required');
        $this->form_validation->set_rules('tanggal_kembali', 'Tanggal_kembali', 'required');
        $this->form_validation->set_rules('status_peminjaman', 'Status_peminjaman', 'required');

      if( $this->form_validation->run() == FALSE ) {

      	$this->load->view('Admin/inventaris/ubah',$data);

      }else{

      	$this->Datainventaris_model->ubahdatainventaris();
      	//$this->Datainventaris_model->inventaris();
      	redirect('datainventaris');
      }
		 
   }
}