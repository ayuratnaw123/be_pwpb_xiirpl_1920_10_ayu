<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class datapetugas extends CI_Controller {
	public function __construct()
	{  

		parent :: __construct();
		$this->load->model('Datapetugas_model');
	 	$this->load->library('form_validation');

	} 

    public function index() 
	{
		
		$data['judul'] = 'data petugas';
		$data['petugas'] = $this->Datapetugas_model->getAlldatapetugas();
		if( $this->input->post('keyword')){
			$data['petugas'] = $this->Datapetugas_model->cariDataPetugas();
		}

		$this->load->view('Admin/Datapetugas/index', $data);
	}

	public function hapus($id_petugas)
	{
		$this->Datapetugas_model->hapusDatapetugas($id_petugas);
		redirect('datapetugas');
	}


	public function tambah()

	{

       $data['judul']   = 'form Tambah Data petugas';
       $data['id_level'] = $this->Datapetugas_model->getidlevel();
       $data['nama_level'] = $this->Datapetugas_model->getnamalevel();
        
        $this->form_validation->set_rules('id_level', 'Id_level', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('nama_ptgs', 'Nama_ptgs', 'required');

      if( $this->form_validation->run() == FALSE ) {

      	$this->load->view('Admin/Datapetugas/Tambah', $data);

      }else{

      	$this->Datapetugas_model->tambahdatapetugas();
      	//$this->Datainventaris_model->inventaris();
      	redirect('datapetugas');
      }
		
   }

   public function detail($id) {
	  	$data['judul'] = 'Detail Data petugas';
		  $data['datapetugas'] = $this->Datapetugas_model->getDatapetugasById($id)->result_array();
		// var_dump( $id) ;die();
			$this->load->view('Admin/Datapetugas/detail', $data);
		}


		public function ubah($id_petugas)
		{	
			$data['judul'] = 'Form Ubah Data Petugas';
			//$id_petugas= $this->input->post('id_petugas');
			$data['ptgs'] = $this->Datapetugas_model->getDatapetugasById($id_petugas)->row();
			//var_dump($data['ptgs']);die();

			

			$this->form_validation->set_rules('id_level', 'Id_level', 'required');
			$this->form_validation->set_rules('id_petugas', 'Id_petugas', 'required');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('nama_ptgs', 'Nama_ptgs', 'required');
			$this->form_validation->set_rules('Email', 'Email', 'required');

			if( $this->form_validation->run() == FALSE ) {
			$this->load->view('admin/datapetugas/ubah',$data);
		} else{
			$this->Datapetugas_model->ubah();
		   //$this->session->set_flashdata('flash', 'Diubah');
			redirect('datapetugas');
		}
	}
}





