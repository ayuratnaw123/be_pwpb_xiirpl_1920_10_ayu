<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class listbarang extends CI_Controller {
    public function __construct()
	 {
	 	parent::__construct();
	 	$this->load->model('Barang_model');
	 	// $this->load->library('form_validation');
	 }  

	 public function index()

	{  
		
		$data['judul'] = 'data barang';
		$data['barang'] = $this->Barang_model->getAllbarang();
  //       if( $this->input->post('keyword')){
		// 	$data['barang'] = $this->Barang_model->cariDatabarang();
		// }

		$this->load->view('listbarang/index', $data);
	} 
}
 
 