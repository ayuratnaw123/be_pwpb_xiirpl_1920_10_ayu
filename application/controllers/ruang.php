<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ruang extends CI_Controller {
	public function __construct()
	{

		parent :: __construct();
		$this->load->model('Ruang_model');
	 	$this->load->library('form_validation');

	} 

    public function index()
	{
		
		$data['judul'] = 'data ruang';
		$data['ruang'] = $this->Ruang_model->getAllRuang();
		if( $this->input->post('keyword')){
			$data['ruang'] = $this->Ruang_model->cariDataruang();
		}

		$this->load->view('Admin/ruang/index', $data);
	}
 
	public function tambah()

	{

        $data['judul']   = 'form Tambah Data ruang';
        
        $this->form_validation->set_rules('nama_ruang', 'Nama_ruang', 'required');
        $this->form_validation->set_rules('kode_ruang', 'Kode_ruang', 'required');
        

      if( $this->form_validation->run() == FALSE ) {

      	$this->load->view('Admin/ruang/Tambah');

      }else{

      	$this->Ruang_model->tambahruang();
      	//$this->Datainventaris_model->inventaris();
      	redirect('ruang');
      }
		
   }

   public function hapus($id_ruang)
	{
		$this->Ruang_model->hapusDataruang($id_ruang);
		redirect('ruang');
	}

	}
 