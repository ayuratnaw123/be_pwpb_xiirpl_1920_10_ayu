<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class barang extends CI_Controller {
	 public function __construct() 
 	 {
	 	parent::__construct();
	 	$this->load->model('Barang_model');
	 	$this->load->library('form_validation');
	 }  

	 public function index()

	{  
		
		$data['judul'] = 'data barang';
		$data['barang'] = $this->Barang_model->getAllbarang();
        if( $this->input->post('keyword')){
			$data['barang'] = $this->Barang_model->cariDatabarang();
		}
		$this->load->view('Admin/barang/index', $data);
	}

	public function tambah()

	{
 
       $data['judul']   = 'form Tambah Data barang';
       $data['id_ruang'] = $this->Barang_model->getidruang();
       $data['id_jenis'] = $this->Barang_model->getidjenis();
       $data['nama_jenis'] = $this->Barang_model->getnamajenis();
       $data['nama_ruang'] = $this->Barang_model->getnamaruang();
       // var_dump($data['id_ruang']);die;
       
        $this->form_validation->set_rules('id_jenis', 'id_jenis', 'required');
        $this->form_validation->set_rules('id_ruang', 'id_ruang', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('kondisi', 'kondisi', 'required');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        $this->form_validation->set_rules('kode_inventaris', 'kode_inventaris', 'required');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'required');

      if( $this->form_validation->run() == FALSE ) {

      	$this->load->view('Admin/barang/Tambah', $data);

      }else{

      	$this->Barang_model->tambahdatabarang();
      	//$this->Barang_model->inventaris();
      	redirect('barang');
      }
		
   }

   public function hapus($id_inventaris)
	{
		$this->Barang_model->hapusDatabarang($id_inventaris);
		redirect('barang');
	} 

	public function detail($id) {
	  	$data['judul'] = 'Detail Data barang';
		  $data['databarang'] = $this->Barang_model->getBarangById($id); 
		// var_dump( $id) ;die();
			$this->load->view('Admin/barang/detail', $data);
		}

	public function ubah($id_inventaris)
		{	
		$data['judul'] = 'Form Ubah Data inventaris';
		$data['databarang'] = $this->Barang_model->getBarangById($id_inventaris);
			 //var_dump($data['pm']);die();
			
		
        $this->form_validation->set_rules('nama', 'nama', 'required');
        // $this->form_validation->set_rules('kondisi', 'kondisi', 'required');
        // $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        // $this->form_validation->set_rules('kode_inventaris', 'kode_inventaris', 'required');
        // $this->form_validation->set_rules('keterangan', 'keterangan', 'required');

		
		if( $this->form_validation->run() == FALSE ) {

			$this->load->view('Admin/barang/ubah', $data); 
		} else{
			$this->Barang_model->ubah();
		    // $this->session->set_flashdata('flash', 'Diubah');
			//redirect('barang/index');
		}
	}

	public function cetak(){
		$data['judul'] = 'cetak data';
		$data['barang'] = $this->Barang_model->getAllbarang();
		$this->load->view('Admin/barang/cetak', $data);
	}
}

// 		