<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pengembalian extends CI_Controller { 
	public function __construct() 
	{
 
		parent :: __construct();
		$this->load->model('Pengembalian_model');
	 	$this->load->library('form_validation');

	} 

    public function index()
	{
		
		$data['judul'] = 'data pengembalian';
		$data['ppengembalian'] = $this->Pengembalian_model->getAllPengembalian();
        if( $this->input->post('keyword')){
			$data['pengembalian'] = $this->Pengembalian_model->caripengembalian();
		}
		$this->load->view('Admin/pengembalian/index', $data);
	}



public function tambah()

	{

       $data['judul']   = 'form Tambah Data pengembalian';
       $data['id_inventaris'] = $this->Pengembalian_model->getidinventaris();
       $data['id_peminjam'] = $this->Pengembalian_model->getidpeminjam();
       $data['id_peminjaman'] = $this->Pengembalian_model->getidpeminjaman();
        $data['nama_peminjam'] = $this->Pengembalian_model->getidpeminjam();
        $data['status_peminjaman'] = $this->Pengembalian_model->getstatuspeminjaman();
         $data['nama'] = $this->Pengembalian_model->getnama();
        
        // $this->form_validation->set_rules('id_pengembalian', 'Id_pengembalian', 'required');
        $this->form_validation->set_rules('tanggal_kembali', 'Tanggal_kembali', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
         $this->form_validation->set_rules('id_peminjam', 'Id_peminjam', 'required');
          $this->form_validation->set_rules('id_peminjaman', 'Id_peminjaman', 'required');
        

      if( $this->form_validation->run() == FALSE ) {

      	$this->load->view('Admin/pengembalian/Tambah', $data);

      }else{

      	$this->Pengembalian_model->tambahdatapengembalian();
      	//$this->Datainventaris_model->inventaris();
      	redirect('pengembalianpetugas');
      }
		
   }


   public function ubah(){

   	$this->load->view('Admin/pengembalian/ubah');
   }

   public function detail($id_pengembalian) {
	  	$data['judul'] = 'Detail Data pengembalian';
		$data['ppengembalian'] = $this->Pengembalian_model->getpengembalianById($id_pengembalian);
		// var_dump( $id) ;die();
			$this->load->view('Admin/pengembalian/detail', $data);
		}



	}
