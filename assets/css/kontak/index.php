<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.css">
    
    <!-- Native CSS -->
    <link rel="stylesheet" href="assets/css/kontak/kontak.css">

    <!-- Icon -->
    <link rel="shortcut icon" href="assets/img/logo.jpg">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Hover -->
    <link href="css/hover.css" rel="stylesheet" media="all">

    <title>Simanis | Ekstrakurikuler</title>
</head>
<body>
    <!-- Ini Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark biru fixed-top">
        <a class="navbar-brand" href="#">
            <img src="assets/img/icon-pendaftaran-png-7.png" width="110" class="d-inline-block" alt="">
            PENDAFTARAN EKSKUL
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse putih" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto topnav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Absensi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Raport</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Komunitas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#">Ekskul <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Hubin</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="kemasan">
        <div class="pembungkus">
            <h2>Contact person</h2>
            <hr>
            <center>
                <div class="profile mt-3">
                    <div class="photo">
                        <center>
                            <h3 class="mt-3 mb-4">OPS</h3>
                            <img src="assets/img/45.jpg" width="140">
                        </center>
                    </div>
                    <div class="nomor">
                        <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                        <span><img src="assets/img/122.png" width="45">
                        @ahmadalafrizy</span>
                    </div>
                    <a href="" class="btn btn-success text-light jarak float-right edit" data-toggle="modal" data-target="#editKontak">Edit      <i class="fas fa-edit"></i></a>
                </div>
                <form action="" class="mt-4">
                    <input type="text" name="cari" id="cari" placeholder="Cari Kontak Walimurid.....">
                    <button type="button" class="btn btn-primary btn-lg ml-2 tombol">Search</button>
                </form>
                <div class="bungkus mt-4">
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">Pramuka</h3>
                                <img src="assets/img/900.jpg" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                           
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45">
                            <a href="https://www.instagram.com/ambalanresiden/?hl=id">
                             @ambalanresiden</span></a>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">Drupadi</h3>
                                <img src="assets/img/09.jpg" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45"> 
                            <a href="https://www.instagram.com/drupadi_smkn1bks/?hl=id">
                            @drupadi_smkn1bks</span></a>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">PMR</h3>
                                <img src="assets/img/99.png" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45"><a href="
                                https://www.instagram.com/pmrsmkn1_/?hl=id">

                            @pmrsmkn1_</span></a>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">INORI</h3>
                                <img src="assets/img/images.jpg" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45">
                            <a href="https://www.instagram.com/inosaijfest_/?hl=id">
                             @inosaijfest</span></a>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">ROHIS</h3>
                                <img src="assets/img/78.PNG" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45">

                             @ahmadalafrizy</span>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">BASKET</h3>
                                <img src="assets/img/89.jpeg" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45">
                            <a href="https://www.instagram.com/basketsmkn1bks/?hl=id">
                             @basketsmkn1bks</span></a>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">ANDERPATI</h3>
                                <img src="assets/img/download.png" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45">
                              <a href="https://www.instagram.com/anderpati_/?hl=id">
                             @anderpati</span></a>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">BOXER</h3>
                                <img src="assets/img/logo2.jpg" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45"> 
                            @ahmadalafrizy</span>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">ENGLISH CLUB</h3>
                                <img src="assets/img/9000.jpg" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45">
                            <a href="https://www.instagram.com/ecsmk1bks/?hl=id">
                             @ecsmk1bks</span></a>
                        </div>
                    </div>
                    <div class="profile">
                        <div class="photo">
                            <center>
                                <h3 class="mt-3 mb-4">PASKIBRA</h3>
                                <img src="assets/img/12.jpg" width="140">
                            </center>
                        </div>
                        <div class="nomor">
                            
                            <span><img src="assets/img/wa.png" width="50"> 0895-3967-80183</span><br>
                            <span><img src="assets/img/122.png" width="45">
                            <a href="https://www.instagram.com/paskibrakansas/?hl=id">
                             @paskibrakansas</span>
                        </div></a>
                    </div>
                   
                        </div>
                    </div>
                </div>
            </center>
        </div>
    </div>
        
    <footer>
    <div class="konten1">
        <div class="line"></div>
            <h3>PENDAFTARAN</h3>
            <h3>EKSKUL</h3>
            <P>Aplikasi yang mempermudah Orang<br> Tua dan Guru dalam
            pembagian hasil<br> evaluasi belajar siswa dan berkonsultasi
            secara online/digital
            </P>
        </div>
        <div class="konten2">
            <h5>OUR TEAM</h5>
                <div class="line2">
                    <div class="l-line"></div>
                </div>
                <li><a href="#">Raport</a></li><hr class="line2">
                <li><a href="#">Absensi</a></li><hr class="line2">
                <li><a href="#">Ekskul</a></li><hr class="line2">
                <li><a href="#">Komunitas</a></li><hr class="line2">
                <li><a href="#">Hubin</a></li><hr class="line2">
        </div>
        <div class="konten3">
            <h5>FOLLOW US</h5>
                <div class="line2">
                    <div class="l-line"></div>
                    <li><a href="#"><i class="fab fa-twitter t" style="margin-left: -60px; padding: 10px; border-radius: 100%;"></i></a></li>
                    <li><a href="#"><i class="fab fa-facebook f" style="margin-left: -5px; padding: 10px 12px; border-radius: 100%;"></i></a></li>
                    <li><a href="#"><i class="fab fa-instagram i" style="margin-left: 10px; padding: 10px 12px; border-radius: 100%;"></i></a></li>
                    <li><a href="#"><i class="fab fa-youtube y" style="margin-left: 10px; padding: 10px 10px; border-radius: 100%;"></i></a></li>
                </div>
        </div>
        <div class="konten4">
            <h5>OUR NEWSLETTER</h5>
                <div class="line2">
                    <div class="l-line"></div>
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum totam pariatur dolor atque minus, laborum impedit eligendi vitae recusandae, distinctio neque itaque, fugit magni incidunt harum aliquid tenetur deleniti! Vero?</p> 
        </div>
    <div class="footer-bottom">
        <p>Copyright &copy; 2019 Simanis Production</p>
    </div>
    </footer>

    <!-- Modal Edit Kontak -->
    <div class="modal fade" id="editKontak" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                            <div class="modal-body">
                                <h4 class="modal-title h3 text-center font-weight-normal" id="exampleModalLabel">Edit Agenda Pertemuan</h4>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="formGroupExampleInput1 text-left">WhatsApp</label>
                                        <input type="number" class="form-control" id="formGroupExampleInput">
                                    </div>
                                    <div class="form-group">
                                        <label for="formGroupExampleInput2 text-left">LINE</label>
                                        <input type="teks" class="form-control" id="formGroupExampleInput2">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal <i class="fas fa-ban"></i></button>
                                <button type="button" class="btn btn-primary">Simpan <i class="fas fa-save"></i></button>
                            </div>
                            </div>
                        </div>
                    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>
    
</body>
</html>
